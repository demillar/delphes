#PBS -N generatePileUp
#PBS -l walltime=03:00:00
#PBS -l nodes=4
#!/bin/bash
source /home/dam1g09/.bash_profile
cd /home/dam1g09/delphes/
/home/dam1g09/delphes/DelphesPythia8 cards/converter_card.tcl generatePileUp.cmnd MinBias.root
./root2pileup MinBias.pileup MinBias.root