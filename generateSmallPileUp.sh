#PBS -N generateSmallPileUp
#PBS -l walltime=00:10:00
#PBS -l nodes=4
#!/bin/bash
source /home/dam1g09/.bash_profile
cd /home/dam1g09/delphes/
/home/dam1g09/delphes/DelphesPythia8 cards/converter_card.tcl examples/Pythia8/generatePileUp.cmnd SmallMinBias.root
./root2pileup SmallMinBias.pileup SmallMinBias.root