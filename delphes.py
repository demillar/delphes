#!/usr/bin/env python
# declan.millar@cern.ch

from argparse import ArgumentParser
parser = ArgumentParser(description = "runs delphes with pythia8 via the iridis batch system")
parser.add_argument("filename", default = "", help = "set filename")
parser.add_argument("-w", "--walltime", default = "00:15:00", help = "walltime 'hh:mm:ss'")
parser.add_argument("-c", "--card", default = "delphes_card_ATLAS_PileUp_dilepton.tcl", help = "set filename")
parser.add_argument("-n", "--nevents", default = 10000, help = "set number of events in file")
parser.add_argument("-N", "--nodes", default = 1, help = "set number of nodes")
args = parser.parse_args()

delphes_directory = "/home/dam1g09/delphes/"
pythia_directory = "/home/dam1g09/pythia"
card_directory = "/home/dam1g09/delphes/cards/"
data_directory = "/scratch/dam1g09/zprime/"
suffix = "_delphes"
if "PileUp" in args.card: suffix = suffix + "_pileup"

delphes = "{}DelphesPythia8".format(delphes_directory)
card = "{}{}".format(card_directory, args.card)
input_name = "{}{}".format(data_directory, args.filename)
job_name = "{}{}".format(args.filename, suffix)
output_name = "{}{}{}.root".format(data_directory, args.filename, suffix)
handler_name = "{}{}{}.sh".format(delphes_directory, args.filename, suffix)
log_name = "{}{}{}.log".format(data_directory, args.filename, suffix)
command_name = "{}{}{}.cmnd".format(delphes_directory, args.filename, suffix)

from os import path
from sys import exit
from subprocess import call
if path.isfile(output_name):
    print "WARNING: output file {} already exists. Deleting...".format(output_name)
    call("rm {}".format(output_name), shell = True)
if path.isfile(output_name):
    exit("ERROR: output file {} still exists. Failed to delete.".format(output_name))
    
if path.isfile(input_name + ".lhef"):
    compression = False
elif path.isfile(input_name + ".lhef.gz"):
    compression = True
else:
    exit("ERROR: input file {}.lhef(.gz) not found.".format(input_name))

from StringIO import StringIO
command = StringIO()
print >> command, "Main:numberOfEvents = {}           ! number of events to generate".format(args.nevents)
print >> command, "Main:timesAllowErrors = 3          ! how many aborts before run stops"
print >> command, ""
print >> command, "! 2) Settings related to output in init(), next() and stat()."
print >> command, ""
print >> command, "Init:showChangedSettings = on      ! list changed settings"
print >> command, "Init:showChangedParticleData = off ! list changed particle data"
print >> command, "Next:numberCount = 100             ! print message every n events"
print >> command, "Next:numberShowInfo = 1            ! print event information n times"
print >> command, "Next:numberShowProcess = 1         ! print process record n times"
print >> command, "Next:numberShowEvent = 0           ! print event record n times"
print >> command, ""
print >> command, "! 3) Set the input LHE file"
print >> command, ""
print >> command, "Beams:LHEF = {}".format(input_name + ".lhef")
print >> command, "Beams:frameType = 4"
print >> command, "PDF:pSet = LHAPDF6:CT14llo"

try:
    with open('{}'.format(command_name), 'w') as command_file:
        command_file.write(command.getvalue())
    # print "command file  = {}.".format(command_name)
except OSError:
    exit("Error: Cannot write command file.")

handler = StringIO()
print >> handler, "#PBS -N {}".format(job_name)
print >> handler, "#PBS -l walltime={}".format(args.walltime)
print >> handler, "#PBS -l nodes={}".format(args.nodes)
print >> handler, "#!/bin/bash"
print >> handler, "cd {}".format(delphes_directory)
print >> handler, "export PYTHIA8={}".format(pythia_directory)
print >> handler, "export LD_LIBRARY_PATH='$LD_LIBRARY_PATH:{}/lib'".format(pythia_directory)
print >> handler, "source /home/dam1g09/.bash_profile"
if compression: print >> handler, "gzip -d {}.lhef.gz > {}".format(input_name, log_name)
print >> handler, "{} {} {} {} >> {}".format(delphes, card, command_name, output_name, log_name)
if compression: print >> handler, "gzip {}.lhef >> {}".format(input_name, log_name)

try:
    with open('{}'.format(handler_name), 'w') as handler_file:
        handler_file.write(handler.getvalue())
    # print "handler  = {}.".format(handler_name)
except OSError:
    exit("Error: Cannot write handler file.")

print "submitting job {} with {} walltime ...".format(job_name, args.walltime)
call("chmod a+x {}".format(handler_name), shell = True)
call('qsub {}'.format(handler_name), shell = True)
